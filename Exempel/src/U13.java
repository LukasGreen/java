import java.util.Scanner;
public class U13 {

	public static void main(String[] args) {
		Scanner read = new Scanner(System.in);
		
		String[] months = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		int[] monthDays = {31, 28, 31, 30, 31, 30, 31, 30, 31, 30, 31, 30};
		
		int month = read.nextInt();
		
		System.out.println("Månaden är " + months[month-1] + " och den har " + monthDays[month-1] + " dagar");
		
		
		

	}

}
