import java.util.Scanner;

public class Test {

	public static int kvadrat(int n) {
		return n * n;
	}

	public static void main(String[] args) {
		String namn;
		int tal;
		Scanner read = new Scanner(System.in);
		//stäng inte systemo 

		System.out.println("Ange ett namn");
		namn = read.nextLine(); // Läser in hel rad
		System.out.println("Namnet är " + namn);
		
		System.out.println("Ange ett tal");
		tal = read.nextInt(); // Läser in heltad
		System.out.println("Kvadraten av " + tal + " är " + kvadrat(tal));
		
		read.nextLine(); // Tömmer inläsningsbufferten
		
		
		System.out.println("Ange ett tal");
		tal = read.nextInt();
		System.out.println("Kvadraten av " + tal + " är " + kvadrat(tal));
		
		
	

}
}
