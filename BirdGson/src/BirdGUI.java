import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;

/**
 * GUI for a program that searches birds.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class BirdGUI {
    
   	/**
     * Minimal frame height.
     */
    private static final int FRAME_MIN_HEIGHT = 250;
    
    /**
     * Minimal frame width.
     */
    private static final int FRAME_MIN_WIDTH = 600;
    
    /**
     * Initial frame height.
     */
    private static final int FRAME_HEIGHT = 500;
    
    /**
     * Initial frame width.
     */
    private static final int FRAME_WIDTH = 700;

    /**
     * Width of input field.
     */
    private static final int INPUT_FIELD_WIDTH = 30;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_1 = 5;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_2 = 10;
    
    /**
     * Message with instructions to display in the text area.
     */
    private static final String INSTRUCTION_MESSAGE = "Search for birds.\n\n";

    /**
     * Text next to the input field.
     */
    private static final String INPUT_LABEL_TEXT = "Search: ";
    
    /**
     * The model that handles the communication and data.
     */
    private BirdModel model;
    
    /**
     * The frame.
     */
    private JFrame frame;
        
    /**
     * Text area for results.
     */
    private JTextArea messageArea;
    
    /**
     * Input field.
     */
    private JTextField searchInputField;
    
    /**
     * Creates a new GUI for a program that searches for birds.
     * @param model The model that handles the communication and data.
     */
    public BirdGUI(BirdModel model) {
        super();
        this.model = model;
        setUpFrame();
        searchInputField.requestFocusInWindow();
    }

    /**
     * Sets up the frame with three areas.
     */
    private void setUpFrame() {
        frame = new JFrame("Birds");
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.setMinimumSize(new Dimension(FRAME_MIN_WIDTH,FRAME_MIN_HEIGHT));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(setUpInputPanel(), BorderLayout.NORTH);
        frame.add(setUpScrollArea(), BorderLayout.CENTER);
        frame.add(setUpBottomPanel(), BorderLayout.SOUTH);
        frame.setVisible(true);
    }
    
    /**
     * Sets up an input panel with a label, an input field and a button.
     * @return Input panel.
     */
    private JPanel setUpInputPanel() {
        JLabel inputLabel = new JLabel(INPUT_LABEL_TEXT);
        
        searchInputField = new JTextField(INPUT_FIELD_WIDTH);
        searchInputField.setBackground(Color.WHITE);
        searchInputField.setBorder(createBorder(PADDING_1));
        searchInputField.addActionListener(new InputFieldListener());
        
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(new InputFieldListener());
        
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new FlowLayout());
        inputPanel.setBackground(Color.LIGHT_GRAY);
        inputPanel.setBorder(createBorder(PADDING_2)); 
        inputPanel.add(inputLabel);
        inputPanel.add(searchInputField);
        inputPanel.add(searchButton);
        return inputPanel;
    }

    /**
     * Sets up a scroll area with a text area.
     * @return Scroll area.
     */
    private JScrollPane setUpScrollArea() {
        messageArea = new JTextArea(INSTRUCTION_MESSAGE);
        messageArea.setBackground(Color.WHITE);
        messageArea.setEditable(false);
        
        JScrollPane scrollArea = new JScrollPane(messageArea);
        scrollArea.setBackground(Color.WHITE);
        scrollArea.setBorder(createBorder(PADDING_2));
        return scrollArea;
    }
    
    /**
     * Sets up a bottom panel with a clear button.
     * @return Bottom panel.
     */
    private JPanel setUpBottomPanel() {
        JButton clearButton = new JButton("Clear");
        clearButton.addActionListener(new ClearListener());
        
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());
        bottomPanel.setBackground(Color.LIGHT_GRAY);
        bottomPanel.setBorder(createBorder(PADDING_2));
        bottomPanel.add(clearButton);
        return bottomPanel;
    }

    /**
     * Creates a border with an empty border as padding.
     * @param padd Width of empty border.
     * @return Border with padding.
     */
    private CompoundBorder createBorder(int padd) {
        return BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(padd,padd,padd,padd));
    }
    
    /**
     * Listener for the search button and the input field.
     * @version 2.2 2021-01-26
     * @author Daniel Viström
     */
    private class InputFieldListener implements ActionListener {
       
       /**
        * Creates a new InputFieldListener.
        */
       public InputFieldListener() {
           super();
       }

       /**
        * Action to perform when search button or enter is pressed.
        * @param event The event that happened.
        */
       public void actionPerformed(ActionEvent event){
    	  String searchString = searchInputField.getText();
    	  model.clearSearch();
    	  if (! searchString.equals("")){
    		  messageArea.append("Du söker efter: " + searchString + "\n");
    		try {
				messageArea.append(model.searchStringPost(searchString) + "\n");
				messageArea.append("\n");
			} catch (IOException e) {
				JOptionPane.showMessageDialog(frame, "Det går inte att söka nu.");
			}
    	  }
          searchInputField.setText("");
          searchInputField.requestFocusInWindow();
       }
    }
    
    /**
     * Listener for the clear button.
     * @version 2.2 2021-01-26
     * @author Daniel Viström
     */
    private class ClearListener implements ActionListener {
       
       /**
        * Creates a new ClearListener.
        */
       public ClearListener() {
           super();
       }

       /**
        * Action to perform when clear button is pressed.
        * @param event The event that happened.
        */
       public void actionPerformed(ActionEvent event){
    	   messageArea.setText(INSTRUCTION_MESSAGE);
    	   model.clearSearch();
           searchInputField.requestFocusInWindow();
       }
    }
} 	