import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * List of searchresults in the form of Bird-objects.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class BirdList {

	/**
	 * List containing Bird.
	 */
	@SerializedName("birds")
	@Expose
	private ArrayList<Bird> birds;

	/**
	 * Creates a new list of Bird.
	 */
	public BirdList(){
		birds = new ArrayList<Bird>();
	}

	/**
	 * Creates a string of the list of Bird.
	 * @return The list of Bird as a string.
	 */
	public String toString() {
		String resultString = "";
		if (birds.get(0).getBirdId() == -1) {
			resultString = birds.get(0).getBirdName();
		} else {
			birds.sort(null);
			for (int i = 0; i < birds.size(); i++) {
				resultString += "Bird ID: " + birds.get(i).getBirdId() + "     Bird name: " + birds.get(i).getBirdName()
						+ "\n";
			}
		}
		return resultString;
	}

	/**
	 * Empties the list.
	 */
	public void clear() {
		birds = new ArrayList<Bird>();
	}

}