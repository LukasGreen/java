import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 * Model for a program that searches for birds.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class BirdModel {

	/**
	 * BirdList with the searchresults.
	 */
	private BirdList birdList;

	/**
	 * Creates a new model for a program that searches for birds.
	 */
	public BirdModel() {
		super();
		birdList = new BirdList();
	}

	/**
	 * A searchstring is sent to $_GET at the server and the result is a BirdList.
	 * @param searchString String to search for.
	 * @return String with results.
     * @throws IOException Error in reading from server.
	 */
	public String searchStringGet(String searchString) throws IOException {
		String url = "https://student.oedu.se/~daniel/birds/get_test.php?name=" + searchString;
		HttpUtility.sendGetRequest(url);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		// Datat kommer som en JSONsträng.
		/* T.ex. vid sökning på "svan":
		 *  {"birds":[
		 *   {"BirdID":"37","Name":"Kn\u00f6lsvan","Sort":"3700"},
		 *   {"BirdID":"38","Name":"Mindre s\u00e5ngsvan","Sort":"3800"},
		 *   {"BirdID":"39","Name":"S\u00e5ngsvan","Sort":"3900"},
		 *   {"BirdID":"323","Name":"Sidensvans","Sort":"32300"}
		 * ]}
		 */
		// Parsar datan till ett BirdList-objekt.
		Gson g = new Gson();
		birdList = g.fromJson(response, BirdList.class);
		return birdList.toString();
	}
	
	// Om datan ska skickas med $_POST istället.
	/**
	 * A searchstring is sent to $_POST at the server and the result is a BirdList.
	 * @param searchString String to search for.
	 * @return String with results.
     * @throws IOException Error in reading from server.
	 */
	 public String searchStringPost(String searchString) throws IOException {
	    String url = "https://student.oedu.se/~daniel/birds/post_test.php";
	 	Map<String, String> params = new HashMap<String, String>();
		params.put("name", searchString);  // Går att lägga in flera saker om man vill det.
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		birdList = g.fromJson(response, BirdList.class);
		return birdList.toString();
	 }

	/**
	 * Empties the searchresult.
	 */
	public void clearSearch() {
		birdList.clear();		
	}

}
