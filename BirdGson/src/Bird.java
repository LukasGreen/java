import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Data for a bird.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class Bird implements Comparable<Bird> {

	/**
	 * Id for the bird.
	 */
	@SerializedName("BirdID")
	@Expose
	private int birdId;

	/**
	 * Name of the bird.
	 */
	@SerializedName("Name")
	@Expose
	private String birdName;

	/**
	 * Sorting-number for the bird.
	 */
	@SerializedName("Sort")
	@Expose
	private int sort;

	/**
	 * Creates a new BirdData.
	 */
	public Bird() {
		this.setBirdId(-1);
		this.setBirdName("");
		this.setSort(0);
	}

	/**
	 * Get the Id for the bird.
	 * @return Id for the bird.
	 */
	public int getBirdId() {
		return birdId;
	}

	/**
	 * Sets the Id for the bird.
	 * @param birdId Id for the bird.
	 */
	public void setBirdId(int birdId) {
		this.birdId = birdId;
	}

	/**
	 * Gets the name for the bird.
	 * @return Name for the bird.
	 */
	public String getBirdName() {
		return birdName;
	}

	/**
	 * Sets the name for the bird.
	 * @param birdName The name for the bird.
	 */
	public void setBirdName(String birdName) {
		this.birdName = birdName;
	}

	/**
	 * Gets the sorting-number for the bird.
	 * @return Sorting-number for the bird.
	 */
	public int getSort() {
		return sort;
	}

	/**
	 * Sets the sorting-number for the bird.
	 * @param sort The sorting number for the bird.
	 */
	public void setSort(int sort) {
		this.sort = sort;
	}

	/**
	 * Compares two birds according to sorting-number.
	 * @param bird The bird to compare with.
	 * @return negative number if less, positive number if greater than, zero if equal.
	 */
	@Override
	public int compareTo(Bird bird) {
		return this.getSort() - bird.getSort();
	}

}