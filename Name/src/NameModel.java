/**
 * A model that can run tests, keeps track of the results and can 
 * return them as a string.
 * @version 1.4 2023-01-27
 * @author Daniel Viström
 */
public class NameModel {
    
	/**
	 * List of the names in TEINF3;
	 */
	private static final String[] nameList = {"Henrik", "Lukas", "Emilia", "Simon", "Isak", "Wilma"};
	
	/**
	 * Number of tests that has succeded.
	 */
	private int success;
	
	/**
	 * Number of tests that has failed.
	 */
	private int fail;
	
	/**
	 * Testresult.
	 */
	private String message;
	
    /**
     * Creates a new model.
     */
    public NameModel() {
        super();
        this.success = 0;
        this.fail = 0;
        this.message = "";
    }
    
    /**
     * Clears the list of results.
     */
    public void clearResult() {
        success = 0;
        fail = 0;
        message = "";
    }
 
    /**
     * Checks if the parameter contains a name matches a name in the list. 
     * @param testName Name to test.
     */
    public void runTest(String testName) {
        boolean found = false;
    	int i = 0;
        while(i<nameList.length){
        	if(nameList[i].equals(testName)){
        	    found = true;
        	}
        	i++;
        }
        if(found){
    		success++;
    		message = "Namnet hittades.";
    	} else {
    		fail++;
        	message = "Namnet hittades inte.";
    	}
    }    
  
    /**
     * Transforms the list of test results into a string.
     * @return The result as a string.
     */
    public String resultToString() {
       return message + " Success: " + success + " Fail: " + fail;
    }
}
