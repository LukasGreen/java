import java.util.Scanner;

public class uppg1 {


	public static void main(String[] args) {
		int carrots = 40;
		boolean done = false;
		
		int torsCarrotsEaten = 0;
		int morsCarrotsEaten = 0;
		
		Scanner read = new Scanner(System.in);
		
		System.out.println("Hur lång tid tar Tor på sig att äta en morot?");
		int torsTid = read.nextInt();
		System.out.println("Hur lång tid tar Mor på sig att äta en morot?");
		int morsTid = read.nextInt();
		
		int mor = morsTid-1;
		int tor = torsTid-1;

		// LOOP SOM ÖKAR SEKUNDEN MED 1*
		while (!done) {
			mor++;
			tor++;

			

			if (mor == morsTid && tor == torsTid) { // torstid och morstid är tiden det tar för de att äta 1 morot

				if (carrots > 1) {
					carrots -= 2;
					mor = 0;
					tor = 0;
					torsCarrotsEaten++;
					morsCarrotsEaten++;
				} else {
					done = true;
					mor = 0;
					tor = 0;
				}
			} 
			
			
			if (tor == torsTid) {
				carrots--;
				tor = 0;
				torsCarrotsEaten++;
			}
			
			if (mor == morsTid) {
				carrots--;
				mor = 0;
				morsCarrotsEaten++;
			}
			
			if (carrots == 0) {
				done = true;
			}
			
		}
		
		System.out.println("Tor " + torsCarrotsEaten + ", Mor " + morsCarrotsEaten);
			
			
			
			
			
		}

	}
