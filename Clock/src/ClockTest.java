/**
 * En klass som testar klassen Clock.
 * @author lg0014
 *
 */
public class ClockTest {
	
	/**
	 * Funktionen som testar klassen Clock.
	 * @param args
	 */
	public static void main(String[] args) {
		
		try {
			Clock test = new Clock(19, 45);
			
			
			System.out.println(test.getTime());
			System.out.println("Expected: 19:45");
			
			test.timeTick();
			System.out.println(test.getTime());			
			System.out.println("Expected: 19:46");
		} catch (IllegalValueException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			Clock test2 = new Clock();
			System.out.println(test2.getTime());
			System.out.println("Expected: 00:00");
			
			try {
				test2.setTime(24, 78);
				
				
				
				
			} catch (IllegalValueException e) {
				System.out.println(e.getMessage());
			}
			
			System.out.println("Expected: error");
			test2.setTime(23, 59);
			System.out.println(test2.getTime());			
			System.out.println("Expected: 23:59");
			test2.timeTick();
			System.out.println(test2.getTime());			
			System.out.println("Expected: 00:00");
			test2.timeTick();
			System.out.println(test2.getTime());			
			System.out.println("Expected: 00:01");

			
			
			
		} catch (IllegalValueException e) {
			System.out.println(e.getMessage());
		}
		
		
		
		
		
		
		
		
		
	}
}
