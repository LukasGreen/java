import java.util.ArrayList;

/**
 * Gör en alarmklocka av klockan. 
 * @author lg0014
 *
 */
public class AlarmClock extends Clock {
	
	/**
	 * En lista av alla alarm som har skapats.
	 */
	ArrayList<String> alarms = new ArrayList<String>();
	
	/**
	 * Ställer en klocka utan tid.
	 * @throws IllegalValueException kastar value när value är utanför vad den får vara. 
	 */
	public AlarmClock() throws IllegalValueException {
		super();	
	}
	
	/**
	 * 
	 * @param hour Timmen på klockan 
	 * @param minute Minuten på klocken
	 * @throws IllegalValueException När value är utanför vad den får vara.
	 */
	public AlarmClock(int hour, int minute) throws IllegalValueException {
		super(hour, minute);
	}

	/**
	 * Ökar klockan tid och sätter också på och tar bort alarm.
	 */
	public void timeTick(){
		super.timeTick();

		for (int n = 0; alarms.size() > n; n++) {
		if (alarms.get(n).equals(this.getTime())) {
			System.out.println("ALARM!!");
			alarms.remove(n);
			n--;
			
		}
		}
			
	}
	
	
	/**
	 * Sätter ett nytt alarm
	 * @param time tiden på klockan
	 */
	public void setAlarm(String time) {
		alarms.add(time);
	}
	
}