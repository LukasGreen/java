/**
 * Exception kastad när value är utanför vad den får vara. 
 * @author lg0014
 *
 */

public class IllegalValueException extends Exception{
	
	
	
	private static final long serialVersionUID = -8371862769624942057L;

	/**
	 * Exception utan meddelande
	 */
	public IllegalValueException() {
		super();
	}
	
	/**
	 * Exception med meddelande
	 * @param message error meddelande.
	 */
	public IllegalValueException(String message) {
		super(message);
	}
}
