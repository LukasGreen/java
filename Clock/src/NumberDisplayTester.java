/**
 * Tests the class NumberDisplay.
 * 
 * @author Daniel
 * 2019-10-10
 */
public class NumberDisplayTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		// Tests exception when maxLimit is not greater than minLimit.
		try{
			NumberDisplay nud=new NumberDisplay(5,1);
			nud.increment();
		}
		catch(IllegalValueException e){
			System.out.println(e.getMessage());			
		}
		System.out.println("Expected: maxLimit not greater than minLimit");
		System.out.println();

		try {
			// Tests that display has 4 digits.
			NumberDisplay nd = new NumberDisplay(3,2000);
			System.out.println(nd.getDisplayValue());
			System.out.println("Expected: 0003");
			System.out.println();
			
			// Tests that the function didWrapAround gives false at the beginning.
			System.out.println(nd.didWrapAround());
			System.out.println("Expected: false");
			System.out.println();
					
			// Tests exception if value to be set is outside the interval 
			// minLimit <= value < maxLimit.
			// In this case 2000 is the maxLimit and should give an exception.
			try{
				nd.setValue(2000);
			}
			catch(IllegalValueException e){
				System.out.println(e.getMessage());
			}
			System.out.println("Expected: Value out of range in display");
			System.out.println();
			
			// Tests if display wraps around properly.
			nd.setValue(1999);
			System.out.println("Value: "+nd.getValue()+"  String: "+nd.getDisplayValue()+
					"  WrapAround: "+nd.didWrapAround());
			nd.increment();
			System.out.println("Value: "+nd.getValue()+"  String: "+nd.getDisplayValue()+
					"  WrapAround: "+nd.didWrapAround());
			System.out.println("Expected: Value: 1999  String: 1999  WrapAround: false");
			System.out.println("Expected: Value: 3  String: 0003  WrapAround: true");
			System.out.println();
			
			// Tests if didWrapAround gives false when value wraps around and then the value
			// is set to minLimit.
			nd.setValue(1999);
			nd.increment();
			System.out.println(nd.didWrapAround());
			nd.setValue(0003);
			System.out.println(nd.didWrapAround());
			System.out.println("Expected: true");
			System.out.println("Expected: false");
			System.out.println();
		} catch (IllegalValueException e1) {
			System.out.println(e1.getMessage());
		}

		
		// Tests that number of digits equals the number of digits in maxLimit-1.
		try {
			NumberDisplay nd2 = new NumberDisplay(1,100);
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 01");
			System.out.println();
			
			// Tests if increment() works.
			nd2.increment();
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 02");
			System.out.println();
		} catch (IllegalValueException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			NumberDisplay nd2 = new NumberDisplay(0,1000);
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 000");
			System.out.println();
			
			// Tests if increment() works.
			nd2.increment();
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 001");
			System.out.println();
		} catch (IllegalValueException e) {
			System.out.println(e.getMessage());
		}
		try {
			NumberDisplay nd2 = new NumberDisplay(0,10);
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 0");
			System.out.println();
			
			// Tests if increment() works.
			nd2.increment();
			System.out.println(nd2.getDisplayValue());
			System.out.println("Expected: 1");
			System.out.println();
		} catch (IllegalValueException e) {
			System.out.println(e.getMessage());
		}
		
	}
}
