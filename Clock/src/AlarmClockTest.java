/**
 * Testar klassen AlarmClock.
 * @author lg0014
 *
 */
public class AlarmClockTest {

	/**
	 * Funktionen som testar klassen AlarmClock.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			AlarmClock test = new AlarmClock(13, 36);
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:37");
			test.setAlarm("13:39");
			
			System.out.println(test.getTime());
			System.out.println("Expected: 13:36");
			test.timeTick();
			System.out.println(test.getTime());
			System.out.println("Expected: alarm!");
			test.timeTick();
			System.out.println(test.getTime());
			test.timeTick();
			System.out.println(test.getTime());
			System.out.println("Expected: alarm!");
			
			test.setTime(13, 36);
			System.out.println(test.getTime());
			System.out.println("Expected: 13:36");
			
			test.timeTick();
			System.out.println(test.getTime());
			System.out.println("Expected: no alarm");
			
			test.setTime(13, 36);
			test.timeTick();
			
			
			
		} catch (IllegalValueException e) {
			e.printStackTrace();
		}

	}

}
