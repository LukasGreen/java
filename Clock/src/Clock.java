/**
 * Programmet gör en klocka utan NumberDisplay
 * @author lg0014
 *
 */
public class Clock {
	private NumberDisplay hours;
	private NumberDisplay minutes;
	private String displayString;


/**
 * 
 * @throws IllegalValueException kastar ett error om value är utanför vad den får vara. 
 */
public Clock() throws IllegalValueException {
	
	hours = new NumberDisplay(0, 24);
	minutes = new NumberDisplay(0, 60);	
}


/**
 * 
 * @param hour Timmen på klockan 
 * @param minute Minuten på klocken
 * @throws IllegalValueException kastar ett error om value är utanför vad den får vara. 
 */
public Clock(int hour, int minute) throws IllegalValueException{
	try {
	
		hours = new NumberDisplay(0, 24);
		minutes = new NumberDisplay(0, 60);	
		
		hours.setValue(hour);
		minutes.setValue(minute);
	} catch(IllegalValueException e) {
		throw new IllegalValueException("MAN U uoutta pocket with this one.");
	}
}

/**
 * ökar tiden på number display
 */
public void timeTick() {
	minutes.increment();
	
	if (minutes.didWrapAround()) {
		hours.increment();
	}
}


/**
 * 
 * @param hour timmen klockan sätts till
 * @param minute minuten klockan sätts till
 * @throws IllegalValueException kastar ett error om value är utanför vad den får vara. 
 */
public void setTime(int hour, int minute) throws IllegalValueException {
	try {
	hours.setValue(hour);
	minutes.setValue(minute);
	} catch(IllegalValueException c) {
		throw new IllegalValueException("detta value accepteras inte här >:(");
	}
}


/**
 * 
 * @return reurnar displayen i en string
 */
public String getTime() {
	updateDisplay();
	return displayString;
}

/**
 * uppdaterar värdet på displayen
 */
private void updateDisplay() {
	displayString = hours.getDisplayValue() + ":" + minutes.getDisplayValue();
}

}