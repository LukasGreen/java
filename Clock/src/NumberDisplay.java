/**
 * En display med nummer på :O
 * @author lg0014
 *
 */
public class NumberDisplay {
	private int minLimit;
	private int maxLimit;
	private int value;
	private boolean wrapped;
	
	/**
	 * Sätter värden för klockan
	 * @param minLimit minsta värde numberdisplayen får ha
	 * @param maxLimit största värde numberdisplayen får ha
	 * @throws IllegalValueException kastar ett error om value är utanför vad den får vara. 
	 */
	public NumberDisplay(int minLimit, int maxLimit) throws IllegalValueException {
		
		if (!(maxLimit > minLimit)) {
			throw new IllegalValueException("Maxlimit måste vara större än minlimit");
		} else {
			this.minLimit = minLimit;
			this.maxLimit = maxLimit;
			this.value = minLimit;
			
		}
		
	}
	/**
	 * 
	 * @return value på displayen. 
	 */
	public int getValue() {
		return this.value;
	}
	
	/**
	 * sätter ny value på displayen 
	 * @param newValue ny value för value 
	 * @throws IllegalValueException kastar ett error om value är utanför vad den får vara. 
	 */
	public void setValue(int newValue) throws IllegalValueException {
		if (newValue < minLimit || newValue > maxLimit-1) {
			throw new IllegalValueException("Värdet måste vara inom det acceptabla fältet");
		}else {
			this.value = newValue;
		}
		
	}
	
	/**
	 * kollar längden på int
	 * @param tal räknetal
	 * @return längden på int 
	 */
	private int getIntLength(int tal) {
        int count = 0;
        while(tal >= 1)
        {
        tal = tal / 10;
        count++;
        }
        return count;
	}
	
	/**
	 * 
	 * @return value på displayen i stringformat
	 */
	public String getDisplayValue() {
		int maxLimitLength = getIntLength(this.maxLimit-1);
		int valueLength = getIntLength(this.value);

        int zeroes = maxLimitLength - valueLength;
         
        String displayValue = "";
        
        if (valueLength == 0) {
        	zeroes--;
        }
        	
        while (zeroes > 0) {	
        	displayValue += "0";
        	zeroes--;
        }
        
        
        return displayValue + this.value; 
	}
	
	/**
	 * ökar value 
	 */
	public void increment() {
		this.value += 1;
		if (this.value == maxLimit) {
			this.value = minLimit;
			wrapped = true;
		} else {
			wrapped = false;
		}
	}
	
	/**
	 * 
	 * @return om värdet på första har gått till 0 
	 */
	public boolean didWrapAround() {
		if (wrapped) {
			wrapped = false;
			return true;
		} return false;
	}
	
	
	
}

