import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Question {

@SerializedName("id")
@Expose
private Integer id;
@SerializedName("question")
@Expose
private String question;
@SerializedName("answer1")
@Expose
private String answer1;
@SerializedName("answer2")
@Expose
private String answer2;
@SerializedName("correct_answer")
@Expose
private String correctAnswer;
@SerializedName("album_id")
@Expose
private Integer albumId;

public Integer getId() {
return id;
}

public void setId(Integer id) {
this.id = id;
}

public String getQuestion() {
return question;
}

public void setQuestion(String question) {
this.question = question;
}

public String getAnswer1() {
return answer1;
}

public void setAnswer1(String answer1) {
this.answer1 = answer1;
}

public String getAnswer2() {
return answer2;
}

public void setAnswer2(String answer2) {
this.answer2 = answer2;
}

public String getCorrectAnswer() {
return correctAnswer;
}

public void setCorrectAnswer(String correctAnswer) {
this.correctAnswer = correctAnswer;
}

public Integer getAlbumId() {
return albumId;
}

public void setAlbumId(Integer albumId) {
this.albumId = albumId;
}

}