import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.lang.Math;

public class Login {

	@SerializedName("code")
	@Expose
	private Integer code;
	@SerializedName("token")
	@Expose
	private String token;

	public Login() {
		super();
		
	}
	
	public Integer getStatusCode() {
		return code;
	}

	public String getAuthToken() {
		return token;
	}
	
	public void setAuthToken() {
		this.token = "";
		for(int n = 0; n < 64; n++) {
			this.token += + (int)(Math.random() * 10);
		}
		
	
	}


}