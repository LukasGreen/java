import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;

/**
 * GUI for a program that :)
 * @version 2.2 2021-01-26
 * @author Lukas Green
 */
public class GlosaGUI {

    /**
     * Initial frame height.
     */
    private static final int FRAME_HEIGHT = 550;
    
    /**
     * Initial frame width.
     */
    private static final int FRAME_WIDTH = 800;

    /**
     * Width of input field.
     */
    private static final int INPUT_FIELD_WIDTH = 30;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_1 = 5;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_2 = 10;
    
    /**
     * The model that handles the communication and data.
     */
    private GlosaModel model;
    
    /**
     * The frame.
     */
    private JFrame frame;
        
	private CardLayout clLogin;

	private JPanel cardPanelLogin;

	private JTextField userInput;

	private JTextField passwInput;

	private JTextField userInput2;

	private JTextField passwInput2;

	private JTextField mailInput;

	private JTextField mailInput2;

	private JPanel allGlosor;

	private JPanel quizCard;
    
    /**
     * Creates a new GUI for a program that searches for birds.
     * @param model The model that handles the communication and data.
     */
    public GlosaGUI(GlosaModel model) {
        super();
        this.model = model;
        setUpFrame();
    }

    /**
     * Sets up the frame with three areas.
     */
    private void setUpFrame() {
        frame = new JFrame("Glosa");
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(setUpCardPanelLogin(),BorderLayout.CENTER);
        frame.setVisible(true);
    }
    
    private JPanel setUpCardPanelLogin() {
    	clLogin = new CardLayout();
    	cardPanelLogin = new JPanel(clLogin);
    	cardPanelLogin.add(setUpLoginCard(),"login");
    	cardPanelLogin.add(setUpLoggedInCard(),"loggedin");
    	cardPanelLogin.add(setUpSignUpCard(), "register");
    	cardPanelLogin.add(setUpSearchGlosaCard(), "glosor");
    	cardPanelLogin.add(setUpQuizCard(), "quiz");
    	clLogin.show(cardPanelLogin, "login");
    	return cardPanelLogin;
    }
    
    
    
    private JPanel setUpSignUpCard() {
    	JPanel signUpCard = new JPanel();
    	signUpCard.setLayout(new BorderLayout());
        signUpCard.add(setUpInfoCard(), BorderLayout.CENTER);
        signUpCard.add(setUpEmptyPanel(), BorderLayout.WEST);
        signUpCard.add(setUpEmptyPanel(), BorderLayout.EAST);
    	return signUpCard;
	}

	private JPanel setUpLoggedInCard() {
    	JPanel loggedInCard = new JPanel();
    	loggedInCard.setLayout(new BorderLayout());
        loggedInCard.add(setUpMainCardPanelLoggedIn(), BorderLayout.CENTER);
        loggedInCard.add(setUpLeftCardPanelLoggedIn(), BorderLayout.WEST);
        loggedInCard.add(setUpRightPanelLoggedIn(), BorderLayout.EAST);
        return loggedInCard;
    }

    private JPanel setUpLoginCard() {
    	JPanel loginCard = new JPanel();
    	loginCard.setBackground(Color.ORANGE);
    	loginCard.setBorder(createBorder(PADDING_2));
    	loginCard.setLayout(new BorderLayout());
    	loginCard.add(setUpLoginButtonPanel(), BorderLayout.CENTER);	
    	loginCard.add(setUpLoginPanel(), BorderLayout.NORTH);	//NOW
    	return loginCard;
    }
    
    private JPanel setUpQuizCard() {
    	quizCard = new JPanel();
    	quizCard.setBackground(Color.ORANGE);
    	quizCard.add(setUpEmptyPanel(), BorderLayout.NORTH);
    	JButton createButtonNext = new JButton("NEXT QUESTION"); //TODO ACTIONLISTENER BLA BLA BLA
    	quizCard.add(createButtonNext);
    	
    	
		return quizCard;
    	
    }
    
    
    private JPanel setUpInfoCard() {
    	JPanel infoCard = new JPanel(new FlowLayout());
    	infoCard.add(setUpCreateAccountPanel());
    	infoCard.setBackground(Color.yellow);
    	
    	
		return infoCard;	
    }
    
    
    
    
    private JPanel setUpCreateAccountPanel() {
    	JPanel createAccountPanel = new JPanel();
    	createAccountPanel.setLayout(new BoxLayout(createAccountPanel, BoxLayout.Y_AXIS));
    	createAccountPanel.setBackground(Color.ORANGE);
    	createAccountPanel.add(setUpUserPanel2());
    	createAccountPanel.add(setUpMailPanel2());
    	createAccountPanel.add(setUpPasswordPanel2());
    	createAccountPanel.add(setUpCreateAccountButtonPanel());
    	
		return createAccountPanel;
    }
    
    
    private JPanel setUpSearchGlosaCard() {
    	JPanel searchGlosaPanel = new JPanel();
    	searchGlosaPanel.setLayout(new BorderLayout());
    	searchGlosaPanel.setBackground(Color.ORANGE);
    	searchGlosaPanel.add(setUpEmptyPanel(), BorderLayout.WEST);
    	searchGlosaPanel.add(setUpEmptyPanel(), BorderLayout.EAST);
    	
    	searchGlosaPanel.add(setUpSearchGlosaGlosorPanel(), BorderLayout.CENTER);
    	//Mannen hur många addar jag :/
		return searchGlosaPanel;
    	
    }
    
    private JPanel setUpSearchGlosaGlosorPanel()  {
    	allGlosor = new JPanel();
    	allGlosor.setLayout(new FlowLayout());
    	allGlosor.setBackground(Color.BLUE);
    	
    	
    	

    	
    	
    	
    	



 	
    	
    	
    	return allGlosor;
    }
    
    
    
    private JPanel setUpLoginPanel() {
    	JPanel loginPanel = new JPanel();
    	loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
    	loginPanel.setBackground(Color.ORANGE);
    	loginPanel.add(setUpUserPanel());
    	loginPanel.add(setUpPasswordPanel());
    	return loginPanel;
    }
    
    
    
    private JPanel setUpUserPanel() {
    	JLabel userLabel = new JLabel("User/email");
    	userInput = new JTextField(INPUT_FIELD_WIDTH);
    	userInput.setBackground(Color.WHITE);
    	userInput.setBorder(createBorder(PADDING_1));
    	userInput.requestFocusInWindow();
    	
    	JPanel userPanel = new JPanel();
    	userPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    	userPanel.setBackground(Color.ORANGE);
    	userPanel.add(userLabel);
    	userPanel.add(userInput);
    	return userPanel;
    }
    
    
    
    private JPanel setUpUserPanel2() {
    	JLabel userLabel2 = new JLabel("Username");
    	userInput2 = new JTextField(INPUT_FIELD_WIDTH);
    	userInput2.setBackground(Color.WHITE);
    	userInput2.setBorder(createBorder(PADDING_1));
    	userInput2.requestFocusInWindow();
    	
    	JPanel userPanel2 = new JPanel();
    	userPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
    	userPanel2.setBackground(Color.ORANGE);
    	userPanel2.add(userLabel2);
    	userPanel2.add(userInput2);
    	return userPanel2;
    }
    
    
    	private JPanel setUpMailPanel() {
    	JLabel mailLabel = new JLabel("           Mail");
    	mailInput = new JTextField(INPUT_FIELD_WIDTH);
    	mailInput.setBackground(Color.WHITE);
    	mailInput.setBorder(createBorder(PADDING_1));
    	
    	JPanel mailPanel = new JPanel();
    	mailPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
    	mailPanel.setBackground(Color.ORANGE);
    	mailPanel.add(mailLabel);
    	mailPanel.add(userInput);
    	return mailPanel;
    }
    	
    	private JPanel setUpMailPanel2() {
    		JLabel mailLabel2 = new JLabel("           Mail");
    		mailInput2 = new JTextField(INPUT_FIELD_WIDTH);
    		mailInput2.setBackground(Color.WHITE);
    		mailInput2.setBorder(createBorder(PADDING_1));
    		
    		JPanel mailPanel2 = new JPanel();
    		mailPanel2.setLayout(new FlowLayout(FlowLayout.CENTER));
    		mailPanel2.setBackground(Color.ORANGE);
    		mailPanel2.add(mailLabel2);
    		mailPanel2.add(mailInput2);
    		return mailPanel2;
    	}
    	
    	
    
	private JPanel setUpPasswordPanel(){
    	JLabel passwLabel = new JLabel("  Password");
    	passwInput = new JTextField(INPUT_FIELD_WIDTH);
    	passwInput.setBackground(Color.WHITE);
    	passwInput.setBorder(createBorder(PADDING_1));
    	JPanel passwordPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    	passwordPanel.setBackground(Color.ORANGE);
    	passwordPanel.add(passwLabel);
    	passwordPanel.add(passwInput);
    	return passwordPanel;
	}
	
	
	private JPanel setUpPasswordPanel2(){
		JLabel passwLabel2 = new JLabel("Password");
		passwInput2 = new JTextField(INPUT_FIELD_WIDTH);
		passwInput2.setBackground(Color.WHITE);
		passwInput2.setBorder(createBorder(PADDING_1));
		JPanel passwordPanel2 = new JPanel(new FlowLayout(FlowLayout.CENTER));
		passwordPanel2.setBackground(Color.ORANGE);
		passwordPanel2.add(passwLabel2);
		passwordPanel2.add(passwInput2);
		return passwordPanel2;
	}
	
	
    private JPanel setUpLoginButtonPanel() {
    	JButton loginButton = new JButton("Login");
    	JButton signUpButton = new JButton("Sign up");
    	loginButton.addActionListener(new LoginListener());
    	signUpButton.addActionListener(new SignUpListener());
    	
    	JPanel loginButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    	loginButtonPanel.setBackground(Color.ORANGE);
    	loginButtonPanel.add(loginButton);
    	loginButtonPanel.add(signUpButton);
    	return loginButtonPanel;
    }
    
    private JPanel setUpCreateAccountButtonPanel() {
    	JButton createAccountButton = new JButton("Create Account");
    	createAccountButton.addActionListener(new createAccountListener());
    	JPanel createAccountButtonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
    	createAccountButtonPanel.setBackground(Color.ORANGE);
    	createAccountButtonPanel.add(createAccountButton);
    	return createAccountButtonPanel;
    }
    


    
    /**
     * Sets up a bottom panel with a clear button.
     * @return Bottom panel.
     */
    private JPanel setUpRightPanelLoggedIn() {
    	JPanel rightPanel = new JPanel();
    	rightPanel.setBackground(Color.YELLOW);
    	rightPanel.setBorder(createBorder(60));
    	return rightPanel;
    }

    private JPanel setUpLeftCardPanelLoggedIn() {
    	JPanel leftPanel = new JPanel();
    	leftPanel.setBackground(Color.BLUE);
    	leftPanel.setBorder(createBorder(60));
    	return leftPanel;
    }
    
    private JPanel setUpMainCardPanelLoggedIn() {
    	JPanel mainPanel = new JPanel();
    	mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));
    	mainPanel.setBackground(Color.DARK_GRAY);
    	mainPanel.add(mainButtonFind());
    	mainPanel.add(mainButtonCreate());
    	mainPanel.add(mainButtonLogOut());
    	mainPanel.setBorder(createBorder(200));
    	return mainPanel;
    }
   
    
    private JPanel setUpEmptyPanel() {
    	JPanel emptyPanel = new JPanel();
    	emptyPanel.setLayout(new BorderLayout());
    	emptyPanel.setBackground(Color.DARK_GRAY);
    	emptyPanel.setBorder(createBorder(60));
    	return emptyPanel;
    }
     

    
 
    


    

    private JButton mainButtonFind() {
    	JButton findButton = new JButton("Hitta glosor");
    	findButton.addActionListener(new FindGlosorListener());
    	
    	
    	return findButton;
    }
    
    private JButton mainButtonCreate() {
    	JButton createButton = new JButton("Skapa glosor");
    	createButton.setPreferredSize(new Dimension(10000, 100));
    	createButton.setMinimumSize(new Dimension(10000, 100));
    	//createButton.addActionListener(new CreateGlosorListener());
    	
    	
    	return createButton;
    }
    
    private JButton mainButtonLogOut() {
    	JButton logOutButton = new JButton("Log out");
    	logOutButton.addActionListener(new BackListener());
    	return logOutButton;
    }
    
    
    
    /**
     * Creates a border with an empty border as padding.
     * @param padd Width of empty border.
     * @return Border with padding.
     */
    private CompoundBorder createBorder(int padd) {
        return BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(padd,padd,padd,padd));
    }
    

    
    private class SignUpListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			clLogin.show(cardPanelLogin, "register");
		}
    	
    }
    
    
    private class BackListener implements ActionListener {
    	
    	public BackListener() {
    		super();
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			clLogin.show(cardPanelLogin, "login");
	    	model.logout();
	    	System.out.println(model.tempToken());
			
		}
    	    	
    }
    
    
    private class createAccountListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			try {
				if(model.createAcc(userInput2.getText(), mailInput2.getText(), passwInput2.getText())) {
					clLogin.show(cardPanelLogin, "login");
				} else {
					JOptionPane.showMessageDialog(frame, "Someone is already using this username or email.");
				}
			} catch (IOException e1) {
				JOptionPane.showMessageDialog(frame, "Something went wrong");	
			}
			userInput.setText("");
			passwInput.setText("");
			
		}
    	
    }
    
    private class LoginListener implements ActionListener {

    	public LoginListener() {
    		super();
    	}
    	
		@Override
		public void actionPerformed(ActionEvent event) {
			try {
			if(model.login(userInput.getText(), passwInput.getText())) {
				clLogin.show(cardPanelLogin, "loggedin");
			}else {
				JOptionPane.showMessageDialog(frame, "Wrong login or password");
			}
			} catch(IOException e) {
				JOptionPane.showMessageDialog(frame, "Something went wrong");
			}
			userInput.setText("");
			passwInput.setText("");
			
			System.out.println(model.tempToken());
		}
    	
    	
    }
    
    private class FindGlosorListener implements ActionListener {
    	
    	public FindGlosorListener() {
    		super();
    	}
    	
    	
		@Override
		public void actionPerformed(ActionEvent e) {
			clLogin.show(cardPanelLogin, "glosor");
	    	
			
			try {
				ArrayList<Album> albums = model.getAlbums();
				
				
		    	for(int n = 0; n < albums.size(); n++) {
		        	JButton createButton = new JButton(albums.get(n).getName());
		        	allGlosor.add(createButton);
		        	createButton.addActionListener(new GoToQuizListener(albums.get(n)));
		    	}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
	    		
		}
    }
    
    public class GoToQuizListener implements ActionListener {
    	
    	Album album;
    	public GoToQuizListener(Album album) {
    		super();
    		this.album = album;
    	}

    	
		public void actionPerformed(ActionEvent e) {
			clLogin.show(cardPanelLogin, "quiz");
			System.out.println(album.getName()); //namnet på album som jag klickat in på
			
			
			
			try {
				
				
				ArrayList<Question> questions = model.getQuestions("" + album.getId());
				int n = 0;
				while (n < questions.size()) 
				{
					
				
				JButton createButton = new JButton("" + questions.get(n).getAnswer1()); //du behöver loopa alla frågor ye (0)
				JButton createButton2 = new JButton("" + questions.get(n).getAnswer2()); //du behöver loopa alla frågor ye (0)
				JButton createButton3 = new JButton("" + questions.get(n).getCorrectAnswer()); //du behöver loopa alla frågor ye (0)
				JLabel createQuestion = new JLabel("" + questions.get(n).getQuestion()); 
				
				
				quizCard.add(createQuestion);
				quizCard.add(createButton);
				quizCard.add(createButton2);
				quizCard.add(createButton3);
				
				
				}
				
				
				
				
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			
			

			
			//PRINT LE QUESTIONES :)
			
			
			
		}
    	
    }
    
} 	