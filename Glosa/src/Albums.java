import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Albums {

@SerializedName("albums")
@Expose
private ArrayList<Album> albums;
@SerializedName("code")
@Expose
private Integer code;

public ArrayList<Album> getAlbums() {
return albums;
}

public void setAlbums(ArrayList<Album> albums) {
this.albums = albums;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

}