import java.util.ArrayList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Questions {

@SerializedName("questions")
@Expose
private ArrayList<Question> questions;
@SerializedName("code")
@Expose
private Integer code;

public ArrayList<Question> getQuestions() {
return questions;
}

public void setQuestions(ArrayList<Question> questions) {
this.questions = questions;
}

public Integer getCode() {
return code;
}

public void setCode(Integer code) {
this.code = code;
}

}