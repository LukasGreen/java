	/**
	 * Class containing the main method.
	 * @version 2.2 2021-01-26
	 * @author Lukas Green
	 */
public class GlosaStart {

		/**
	     * Main method that starts the program.
	     * @param args
	     */
		public static void main(String[] args) {
			new GlosaGUI(new GlosaModel());
		}

	}
