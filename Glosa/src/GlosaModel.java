import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 * Model for a program that
 * 
 * @version 2.2 2021-01-26
 * @author Lukas Green
 */
public class GlosaModel {

	/**
	 * BirdList with the searchresults.
	 */

	private Login login;
	private Albums albums;
	private Questions questions;

	/**
	 * Creates a new model for a program that searches for birds.
	 */
	public GlosaModel() {
		super();
	}



	public ArrayList<Album> getAlbums() throws IOException {
		String url = "https://student.oedu.se/~lg0014/glosa/albums/index";
		Map<String, String> params = new HashMap<String, String>();
		params.put("token", login.getAuthToken());
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		this.albums = g.fromJson(response, Albums.class); // TODO bro wtf
		return this.albums.getAlbums();

	}
	
	public ArrayList<Question> getQuestions(String id) throws IOException {
		String url = "https://student.oedu.se/~lg0014/glosa/albums/questions";
		Map<String, String> params = new HashMap<String, String>();
		params.put("token", login.getAuthToken());
		params.put("id", id);
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		this.questions = g.fromJson(response, Questions.class); // TODO bro wtf
		return this.questions.getQuestions();
	}

	public boolean login(String email, String password) throws IOException {
		String url = "https://student.oedu.se/~lg0014/glosa/users/login";
		Map<String, String> params = new HashMap<String, String>();
		params.put("email", email);
		params.put("password", password);
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		this.login = g.fromJson(response, Login.class);
		return this.login.getStatusCode() == 200;

	}

	public boolean createAcc(String username, String email, String password) throws IOException {
		String url = "https://student.oedu.se/~lg0014/glosa/users/create";
		Map<String, String> params = new HashMap<String, String>();
		params.put("username", username);
		params.put("email", email);
		params.put("password", password);
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		this.login = g.fromJson(response, Login.class);
		return this.login.getStatusCode() == 200;

	}

	public void logout() {
		if (this.login == null) {
			return;
		}
		this.login.setAuthToken();
	}

	public String tempToken() {
		if (this.login == null) {
			return "bossman you have no token rn";
		}
		return this.login.getAuthToken();
	}

}
