import java.util.InputMismatchException;
import java.util.Scanner;

public class test {

	public static void main(String[] args) {

		int tal = 0;
		Scanner read = new Scanner(System.in);
		boolean again = true;

		while (again) {
			try {
				System.out.println("Ange ett heltal");
				tal = read.nextInt();

				
				again = false;
			} catch (InputMismatchException e) {
					System.out.println("Felaktig inmatning"	);
					read.nextLine();	//Tömmer inlässningsströmmen
			}

		}
		System.out.println("Tal: " + tal);
		
	}

}
