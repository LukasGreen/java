import java.util.NoSuchElementException;

public class MyQueue implements IntQueue {

	int[] q;

	public MyQueue() {
		q = new int[0];
	}

	public void enqueue(int tal) {
		int[] holder = q;
		q = new int[holder.length + 1];
		for (int i = 0; i < holder.length; i++) {
			q[i] = holder[i];
		}
		q[holder.length] = tal;		

	}

	@Override
	public int dequeue() throws NoSuchElementException {
		if (q.length == 0) {
			throw new NoSuchElementException();
		}
		int head = q[0];
		int[] holder = q;
		q = new int[holder.length - 1];
		for (int i = 1; i < holder.length; i++) {
			q[i - 1] = holder[i];
		}
		return head;
	}

	@Override
	public int peek() throws NoSuchElementException {
		if (q.length == 0) {
			throw new NoSuchElementException();
		}
		return q[0];
	}

	@Override
	public boolean empty() {
		if (q.length > 0) {
			return false;
		} else {
			return true;
		}
	}
}
