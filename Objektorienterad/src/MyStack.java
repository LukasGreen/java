import java.util.NoSuchElementException;

public class MyStack implements IntStack {

	int[] q;

	public MyStack() {
		q = new int[0];
	}

	public void push(int tal) {
		int[] holder = q;
		q = new int[holder.length + 1];
		for (int i = 0; i < holder.length; i++) {
			q[i] = holder[i];
		}
		q[holder.length] = tal;

	}

	@Override
	public int pop() throws NoSuchElementException {
		if (q.length == 0) {
			throw new NoSuchElementException();
		}
		int head = q[q.length-1];
		int[] holder = q;
		q = new int[holder.length - 1];
		for (int i = 0; i < holder.length-1; i++) {
			q[i] = holder[i];
		}
		return head;
	}

	@Override
	public int peek() throws NoSuchElementException {
		if (q.length == 0) {
			throw new NoSuchElementException();
		}
		return q[q.length-1];
	}

	@Override
	public boolean empty() {
		if (q.length > 0) {
			return false;
		} else {
			return true;
		}
	}
}
