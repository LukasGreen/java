/**
 * Test of the class Person.
 * @author LG0014
 *
 */
public class PersonTest {

	/**
	 * Tests the class person
	 * @param args ?
	 */
	public static void main(String[] args) {
		Person person1 = new Person("Bertil Bertilsson", 52);
		Person person2 = new Person("Lisa Berg", 32);
		Person person3 = new Person();
		
		System.out.println(person1.getName());
		System.out.println("Expected: Bertil Bertilsson");
		
		person2.birthday();
		System.out.println(person2.getAge());
		System.out.println("Expected: 33");
		
		System.out.println(person3.getName());
		System.out.println("Expected: Anonym");
		
		person3.setName("Magnus Magnusson");
		person3.printInfo();
		System.out.println("Expected: Namn: Magnus Magnusson Ålder: 1");
		
	
		
	}

}
