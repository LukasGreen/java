/**
 * A person with name and age
 * @author LG0014
 */
public class Person {


		//Attribut. Ska vara private
		/**
		 * Name of person
		 */
		private String name;
		
		/**
		 * Age of person
		 */
		private int age; 
		
		//Konstruktor (körs då objektet skapas)
		
		/**
		 * Creates a new person
		 * @param name Name of person
		 * @param age Age of person
		 */
		public Person(String name, int age) {
			this.name = name;
			this.age = age;
		}
		
		
		//Överlagring av konstruktor 
		
		/**
		 * Creates an anonymous person with age 1.
		 */
		public Person() {
			this("Anonym", 1);
		}
		
		//set-metod (mutator)
		
		/**
		 * Increases age by 1.
		 */
		public void birthday() {
			this.age++;
		}
		
		//get-metod (select)
		
		/**
		 * Gets the age of the person.
		 * @return The age of the person.
		 */
		public int getAge() {
			return this.age;
		}
		
		/**
		 * Charnges the name of the person.
		 * @param name New name of the person.
		 */
		public void setName(String name) {
			this.name = name;
		}
		
		/**
		 * Gets the name of the person
		 * @return the name of the person.
		 */
		public String getName() {
			return this.name;
		}
		
		/**
		 * Prints info about the person.
		 */
		public void printInfo() {
			System.out.println("Namn: " + this.name); 
			System.out.println("Ålder: " + this.getAge());
		}
		
		


}
