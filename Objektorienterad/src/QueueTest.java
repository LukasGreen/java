import java.util.NoSuchElementException;

public class QueueTest {

	public static void main(String[] args) {
		
			IntQueue a = new MyQueue();
			a.enqueue(3);
			System.out.println(a.peek()); 
			System.out.println("expected: 3");
			
			System.out.println(a.empty());			
			System.out.println("expected: false");
			
			try {
			a.dequeue();
			System.out.println(a.peek());
		} catch (NoSuchElementException c){
			System.out.println("ERROR! dis no exist");
		}
			System.out.println("expected: error ");
			
			System.out.println(a.empty());			
			System.out.println("expected: true");


		
	}

}
