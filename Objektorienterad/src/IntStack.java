import java.util.NoSuchElementException;

public interface IntStack {
 
	// Inserts the specified element into the end of this queue.
	void push(int tal);
 
	// Returns the head of this queue and removes it. 
	// Throws an exception if this queue is empty.
	int pop() throws NoSuchElementException;
 
	// Retrieves, but does not remove, the head of this queue.
	// Throws an exception if this queue is empty.
	int peek() throws NoSuchElementException;
 
        // Checks if this queue is empty.
        boolean empty();
}