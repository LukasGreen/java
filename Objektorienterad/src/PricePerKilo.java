import javax.swing.JOptionPane;

public class PricePerKilo {
	
	public static void main(String[] args) {
		String weightTxt = JOptionPane.showInputDialog("Ange vikten på proukter (kg) ");
		String priceTxt = JOptionPane.showInputDialog("Ange priset på produkten (kr) ");
		double weight = Double.parseDouble(weightTxt);
		double price = Double.parseDouble(priceTxt);
		double pricePerKilo = price/weight;
		JOptionPane.showMessageDialog(null, "Jämförpriset = " + pricePerKilo + " kr/kg");
		
	}
}
