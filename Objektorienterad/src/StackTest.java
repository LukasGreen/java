import java.util.NoSuchElementException;

public class StackTest {

	public static void main(String[] args) {
		
			IntStack a = new MyStack();
			a.push(3);
			System.out.println(a.peek()); 
			System.out.println("expected: 3");
			
			System.out.println(a.empty());			
			System.out.println("expected: false");
			
			try {
			a.pop();
			System.out.println(a.peek());
		} catch (NoSuchElementException c){
			System.out.println("ERROR! dis no exist");
		}
			System.out.println("expected: error ");
			
			System.out.println(a.empty());			
			System.out.println("expected: true");


		
	}

}
