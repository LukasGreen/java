public class Index {

	public static void main(String[] args) {
		
		System.out.println("Skapar en tärning och skriver ut den");
		  Die die1 = new Die();
		  die1.printDie();
		
		System.out.println("Skapar en kopp med 3 tärningar och skriver ut koppen");
		  Cup cup = new Cup(3);
		  cup.printCup();

		  System.out.println("Lägger till två tärningar och skriver ut koppen igen");
		  cup.addDie();
		  cup.addDie();
		  cup.printCup();
		  
		  
		  System.out.println("Slår alla tärningar och skriver ut koppen igen");
		  cup.roll();
		  cup.printCup();
		  System.out.println("Summan av alla tärningar i koppen blir: " + cup.sum());

		  System.out.println("Tar bort 3 tärningar och skriver ut koppen igen");
		  cup.remove_die();
		  cup.remove_die();
		  cup.remove_die();
		  cup.printCup();
		  
		  
		  
		  System.out.println("Tar bort 3 tärningar till fast koppen bara har 2, skriver ut koppen igen");
		  for(int i = 0; i < 3; i++) {
		  if (false == cup.remove_die()) {
			  System.out.println("Koppen är redan tom, finns inget att ta bort");
		  }
		  }
		  
		  if (false == cup.printCup()) {
			  System.out.println("Tom kopp!");
		  }
		  System.out.println("Nu är klasserna die och cup testade :-) ");

		
	}

}
