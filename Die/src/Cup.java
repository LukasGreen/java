import java.util.ArrayList;

public class Cup {
	private ArrayList<Die> arr;

	public Cup(int x) {
		arr = new ArrayList<Die>();
		while (x > 0) {
			this.arr.add(new Die());
			x--;
		}
	}

	public boolean printCup() {
		int i = this.arr.size();
		if (i == 0) {
			return false;
		}
		int n = 0;
		while (n < this.arr.size()) {
			this.arr.get(n).printDie();
			n++;
		}
		return true;
	}

	public void addDie() {
		this.arr.add(new Die());
	}

	public void roll() {
		int i = 0;
		while (i < this.arr.size()) {
			this.arr.get(i).roll();
			i++;
		}
	}



	public int sum() {
    int i = 0;
    int sum = 0;
    while (i < this.arr.size()) {
      sum += this.arr.get(i).value();
     i++;
    }
    return sum;
  }

	public boolean remove_die() {
		int i = this.arr.size();
		if (i == 0) {
			return false;
		}
		this.arr.remove(i-1);
		return true;
	}

}
