
public class Die {

  private int n;

  public Die(){
    this.roll();
  }

  public void printDie() {
    System.out.println(this.n);
  }

  public int value(){
    return this.n;
  }

  public void roll(){
    this.n = (int)(Math.random() * 6 + 1);
  }

}