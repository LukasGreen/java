import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

/**
 * Model for a program that searches for birds.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class BirdModel {

	/**
	 * BirdList with the searchresults.
	 */
	private BirdList birdList;
	
	private Login login;
	/**
	 * Creates a new model for a program that searches for birds.
	 */
	public BirdModel() {
		super();
		birdList = new BirdList();
	}
	
	// Om datan ska skickas med $_POST istället.
	/**
	 * A searchstring is sent to $_POST at the server and the result is a BirdList.
	 * @param searchString String to search for.
	 * @return String with results.
     * @throws IOException Error in reading from server.
	 */
	 public String searchStringPost(String searchString) throws IOException {
	    String url = "https://student.oedu.se/~daniel/birds/post_test.php";
	 	Map<String, String> params = new HashMap<String, String>();
		params.put("name", searchString);  // Går att lägga in flera saker om man vill det.
		params.put("auth_token", login.getAuthToken());
		HttpUtility.sendPostRequest(url, params);
		String response = HttpUtility.readSingleLineRespone();
		HttpUtility.disconnect();
		Gson g = new Gson();
		birdList = g.fromJson(response, BirdList.class);
		return birdList.toString();
	 }

	 public Bird[] getBirdList() {
		 return birdList.getBirdArray();
	 }
	 
	 
	/**
	 * Empties the searchresult.
	 */
	public void clearSearch() {
		birdList.clear();		
	}

	public boolean login(String login, String password) throws IOException {
		 String url = "https://student.oedu.se/~daniel/birds/login.php";
		 	Map<String, String> params = new HashMap<String, String>();
			params.put("login", login); 
			params.put("password", password);
			HttpUtility.sendPostRequest(url, params);
			String response = HttpUtility.readSingleLineRespone();
			HttpUtility.disconnect();
			Gson g = new Gson();
			this.login = g.fromJson(response, Login.class);
			return this.login.getStatusCode() == 200;
		
	}

}
