import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Login {

	@SerializedName("status_code")
	@Expose
	private Integer statusCode;
	@SerializedName("auth_token")
	@Expose
	private String authToken;

	public Login() {
		super();
		
	}
	
	public Integer getStatusCode() {
		return statusCode;
	}

	public String getAuthToken() {
		return authToken;
	}


}