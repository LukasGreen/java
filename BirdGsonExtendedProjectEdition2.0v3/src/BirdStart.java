	/**
	 * Class containing the main method.
	 * @version 2.2 2021-01-26
	 * @author Daniel Viström
	 */
public class BirdStart {

		/**
	     * Main method that starts the program.
	     * @param args
	     */
		public static void main(String[] args) {
			new BirdGUI(new BirdModel());
		}

	}
