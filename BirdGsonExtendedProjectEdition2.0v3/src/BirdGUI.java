import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.CompoundBorder;

/**
 * GUI for a program that searches birds.
 * @version 2.2 2021-01-26
 * @author Daniel Viström
 */
public class BirdGUI {
    
   	/**
     * Minimal frame height.
     */
    private static final int FRAME_MIN_HEIGHT = 250;
    
    /**
     * Minimal frame width.
     */
    private static final int FRAME_MIN_WIDTH = 600;
    
    /**
     * Initial frame height.
     */
    private static final int FRAME_HEIGHT = 500;
    
    /**
     * Initial frame width.
     */
    private static final int FRAME_WIDTH = 700;

    /**
     * Width of input field.
     */
    private static final int INPUT_FIELD_WIDTH = 30;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_1 = 5;
    
    /**
     * Width of empty border to create padding.
     */
    private static final int PADDING_2 = 10;
    
    /**
     * Message with instructions to display in the text area.
     */
    private static final String INSTRUCTION_MESSAGE = "Search for birds.\n\n";

    /**
     * Text next to the input field.
     */
    private static final String INPUT_LABEL_TEXT = "Search: ";
    
    /**
     * The model that handles the communication and data.
     */
    private BirdModel model;
    
    /**
     * The frame.
     */
    private JFrame frame;
        
    /**
     * Text area for results.
     */
    private JTextArea messageArea;
    
    /**
     * Input field.
     */
    private JTextField searchInputField;

	private CardLayout cl;

	private JPanel cardPanel;

	private JLabel idLabel;

	private JLabel nameLabel;

	private JLabel sortLabel;

	private JComboBox<Bird> birdMenu;

	private CardLayout clLogin;

	private JPanel cardPanelLogin;

	private JTextField userInput;

	private JTextField passwInput;
    
    /**
     * Creates a new GUI for a program that searches for birds.
     * @param model The model that handles the communication and data.
     */
    public BirdGUI(BirdModel model) {
        super();
        this.model = model;
        setUpFrame();
        searchInputField.requestFocusInWindow();
    }

    /**
     * Sets up the frame with three areas.
     */
    private void setUpFrame() {
        frame = new JFrame("Birds");
        frame.setSize(FRAME_WIDTH,FRAME_HEIGHT);
        frame.setMinimumSize(new Dimension(FRAME_MIN_WIDTH,FRAME_MIN_HEIGHT));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(setUpCardPanelLogin(),BorderLayout.CENTER);
        frame.setVisible(true);
    }
    
    private JPanel setUpCardPanelLogin() {
    	clLogin = new CardLayout();
    	cardPanelLogin = new JPanel(clLogin);
    	cardPanelLogin.add(setUpLoginCard(),"login");
    	cardPanelLogin.add(setUpLoggedInCard(),"loggedin");
    	clLogin.show(cardPanelLogin, "login");
    	return cardPanelLogin;
    }
    
    private JPanel setUpLoggedInCard() {
    	JPanel loggedInCard = new JPanel();
    	loggedInCard.setLayout(new BorderLayout());
        loggedInCard.add(setUpCardPanel(), BorderLayout.CENTER);
        loggedInCard.add(setUpBottomPanel(), BorderLayout.SOUTH);
        return loggedInCard;
    }

    private JPanel setUpLoginCard() {
    	JPanel loginCard = new JPanel();
    	loginCard.setBackground(Color.ORANGE);
    	loginCard.setBorder(createBorder(PADDING_2));
    	loginCard.setLayout(new BorderLayout());
    	loginCard.add(setUpLoginPanel(), BorderLayout.SOUTH);
    	loginCard.add(setUpEmptyLoginPanel(), BorderLayout.CENTER);
    	return loginCard;
    }
    
    private JPanel setUpEmptyLoginPanel() {
    	JPanel emptyLoginPanel = new JPanel();
    	emptyLoginPanel.setLayout(new FlowLayout());
    	emptyLoginPanel.setBackground(Color.ORANGE);
    	String imagePath = "/thumbs-up_1f44d.png";
    	try {
    		//File url = new File(imagePath);
			URL url = getClass().getResource(imagePath);
    		BufferedImage startImage = ImageIO.read(url);
			JLabel picLabel = new JLabel(new ImageIcon(startImage));
			emptyLoginPanel.add(picLabel);
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
    	
    	
    	return emptyLoginPanel;  
    }
    
    private JPanel setUpLoginPanel() {
    	JPanel loginPanel = new JPanel();
    	loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.Y_AXIS));
    	loginPanel.setBackground(Color.ORANGE);
    	loginPanel.add(setUpUserPanel());
    	loginPanel.add(setUpPasswordPanel());
    	loginPanel.add(setUpLoginButtonPanel());
    	return loginPanel;
    }
    
    
    private JPanel setUpUserPanel() {
    	JLabel userLabel = new JLabel("Login");
    	userInput = new JTextField(INPUT_FIELD_WIDTH);
    	userInput.setBackground(Color.WHITE);
    	userInput.setBorder(createBorder(PADDING_1));
    	userInput.requestFocusInWindow();
    	
    	JPanel userPanel = new JPanel();
    	userPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    	userPanel.setBackground(Color.ORANGE);
    	userPanel.add(userLabel);
    	userPanel.add(userInput);
    	return userPanel;
    }
    
	private JPanel setUpPasswordPanel(){
    	JLabel passwLabel = new JLabel("Password");
    	passwInput = new JTextField(INPUT_FIELD_WIDTH);
    	passwInput.setBackground(Color.WHITE);
    	passwInput.setBorder(createBorder(PADDING_1));
    	
    	JPanel passwordPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    	passwordPanel.setBackground(Color.ORANGE);
    	passwordPanel.add(passwLabel);
    	passwordPanel.add(passwInput);
    	return passwordPanel;
	}
	
    private JPanel setUpLoginButtonPanel() {
    	JButton loginButton = new JButton("Login");
    	loginButton.addActionListener(new LoginListener());
    	
    	JPanel loginButtonPanel = new JPanel(new FlowLayout(FlowLayout.RIGHT));
    	loginButtonPanel.setBackground(Color.ORANGE);
    	loginButtonPanel.add(loginButton);
    	return loginButtonPanel;
    	
    }

    
    /**
     * Sets up a bottom panel with a clear button.
     * @return Bottom panel.
     */
    private JPanel setUpBottomPanel() {
        JButton clearButton = new JButton("Clear");
        clearButton.addActionListener(new ClearListener());
        
        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout());
        bottomPanel.setBackground(Color.LIGHT_GRAY);
        bottomPanel.setBorder(createBorder(PADDING_2));
        bottomPanel.add(clearButton);
        return bottomPanel;
    }

    private JPanel setUpCardPanel() {
    	cl = new CardLayout();
    	cardPanel = new JPanel(cl);
    	cardPanel.add(setUpSearchCard(),"search");
    	cardPanel.add(setUpDisplayResultCard(),"answer");
    	cardPanel.setBackground(Color.GRAY);
    	cardPanel.setBorder(createBorder(PADDING_2));
    	cl.show(cardPanel, "search");
    	return cardPanel;
    }
    
    private JPanel setUpSearchCard() {
    	JPanel searchCard = new JPanel();
    	searchCard.setLayout(new BorderLayout());
    	searchCard.add(setUpInputPanel(), BorderLayout.NORTH);
    	searchCard.add(setUpEmptyPanel(), BorderLayout.CENTER);
    	return searchCard;
    }
    
    private JPanel setUpEmptyPanel() {
    	JPanel emptyPanel = new JPanel();
    	emptyPanel.setLayout(new BorderLayout());
    	emptyPanel.setBackground(Color.DARK_GRAY);
    	emptyPanel.setBorder(createBorder(PADDING_2));
    	return emptyPanel;
    }
     
    /**
     * Sets up an input panel with a label, an input field and a button.
     * @return Input panel.
     */
    private JPanel setUpInputPanel() {
        JLabel inputLabel = new JLabel(INPUT_LABEL_TEXT);
        
        searchInputField = new JTextField(INPUT_FIELD_WIDTH);
        searchInputField.setBackground(Color.WHITE);
        searchInputField.setBorder(createBorder(PADDING_1));
        searchInputField.addActionListener(new InputFieldListener());
        
        JButton searchButton = new JButton("Search");
        searchButton.addActionListener(new InputFieldListener());
        
        JPanel inputPanel = new JPanel();
        inputPanel.setLayout(new FlowLayout());
        inputPanel.setBackground(Color.LIGHT_GRAY);
        inputPanel.setBorder(createBorder(PADDING_2)); 
        inputPanel.add(inputLabel);
        inputPanel.add(searchInputField);
        inputPanel.add(searchButton);
        return inputPanel;
    }

    private JPanel setUpDisplayResultCard() {
    	JPanel displayResultCard = new JPanel();
    	displayResultCard.setLayout(new BorderLayout());
    	displayResultCard.add(setUpScrollArea(), BorderLayout.CENTER);
    	displayResultCard.add(setUpSidePanel(),BorderLayout.WEST);
    	displayResultCard.add(setUpButtonPanel(), BorderLayout.SOUTH);
    	return displayResultCard;
    }
    
    private JPanel setUpSidePanel() {
    	JPanel sidePanel = new JPanel();
    	sidePanel.setLayout(new BorderLayout());
    	sidePanel.add(setUpInfoPanel(), BorderLayout.NORTH);
    	sidePanel.add(setUpSpacePanel(), BorderLayout.CENTER);
    	return sidePanel;
    }
    
    private JPanel setUpSpacePanel() {
    	JPanel spacePanel = new JPanel();
    	spacePanel.setLayout(new BorderLayout());
    	spacePanel.setBackground(Color.LIGHT_GRAY);
    	return spacePanel;
    }
    
    private JPanel setUpInfoPanel() {
    	JPanel infoPanel = new JPanel();
    	infoPanel.setLayout(new BoxLayout(infoPanel, BoxLayout.Y_AXIS));
    	infoPanel.add(setUpComboPanel());
    	infoPanel.add(setUpIdPanel());
    	infoPanel.add(setUpNamePanel());
    	infoPanel.add(setUpSortPanel());
    	return infoPanel;
    }

    private JPanel setUpComboPanel() {
    	Bird[] birds = {new Bird()};
    	birdMenu = new JComboBox<Bird>(birds);
    	birdMenu.setBackground(Color.WHITE);
    	birdMenu.setBorder(createBorder(PADDING_1));
    	//För att bestämma bredden av comboboxen.
    	Bird prototypeBird = new Bird();
    	prototypeBird.setBirdName("Detta är en prototyp för combobox");
    	birdMenu.setPrototypeDisplayValue(prototypeBird);
    	
    	JButton selectButton = new JButton("Select");
    	selectButton.addActionListener(new SelectListener());
    	
    	JPanel comboPanel = new JPanel();
    	comboPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
    	comboPanel.setBackground(Color.LIGHT_GRAY);
    	comboPanel.add(birdMenu);
    	comboPanel.add(selectButton);
    	return comboPanel;
    	
    	
    	
    	
    }
    
    private JPanel setUpIdPanel() {
    	idLabel = new JLabel("Id:");
    	JPanel idPanel = new JPanel();
    	idPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    	idPanel.setBackground(Color.LIGHT_GRAY);
    	idPanel.add(idLabel);
    	return idPanel;
    }
    
    private JPanel setUpNamePanel() {
    	nameLabel = new JLabel("Name:");
    	JPanel namePanel = new JPanel();
    	namePanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    	namePanel.setBackground(Color.LIGHT_GRAY);
    	namePanel.add(nameLabel);
    	return namePanel;
    }
    
    private JPanel setUpSortPanel() {
    	sortLabel = new JLabel("Sort:");
    	JPanel sortPanel = new JPanel();
    	sortPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    	sortPanel.setBackground(Color.LIGHT_GRAY);
    	sortPanel.add(sortLabel);
    	return sortPanel;
    }
    
    private JPanel setUpButtonPanel() {
    	JButton backButton = new JButton("Back");
    	backButton.addActionListener(new BackListener());
    	
    	JPanel buttonPanel = new JPanel();
    	buttonPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
    	buttonPanel.setBackground(Color.LIGHT_GRAY);
    	buttonPanel.add(backButton);
    	return buttonPanel;
    }
    
    /**
     * Sets up a scroll area with a text area.
     * @return Scroll area.
     */
    private JScrollPane setUpScrollArea() {
        messageArea = new JTextArea(INSTRUCTION_MESSAGE);
        messageArea.setBackground(Color.WHITE);
        messageArea.setEditable(false);
        
        JScrollPane scrollArea = new JScrollPane(messageArea);
        scrollArea.setBackground(Color.WHITE);
        scrollArea.setBorder(createBorder(PADDING_2));
        return scrollArea;
    }
    

    /**
     * Creates a border with an empty border as padding.
     * @param padd Width of empty border.
     * @return Border with padding.
     */
    private CompoundBorder createBorder(int padd) {
        return BorderFactory.createCompoundBorder(
                BorderFactory.createLineBorder(Color.BLACK),
                BorderFactory.createEmptyBorder(padd,padd,padd,padd));
    }
    
    /**
     * Listener for the search button and the input field.
     * @version 2.2 2021-01-26
     * @author Daniel Viström
     */
    private class InputFieldListener implements ActionListener {
       
       /**
        * Creates a new InputFieldListener.
        */
       public InputFieldListener() {
           super();
       }

       /**
        * Action to perform when search button or enter is pressed.
        * @param event The event that happened.
        */
       public void actionPerformed(ActionEvent event){
    	  String searchString = searchInputField.getText();
    	  model.clearSearch();
    	  if (! searchString.equals("")){
    		  messageArea.append("Du söker efter: " + searchString + "\n");
    		try {
				messageArea.append(model.searchStringPost(searchString) + "\n");
				messageArea.append("\n");
				Bird[] birds = {new Bird()};
				birds = model.getBirdList();
				DefaultComboBoxModel<Bird> dcbm = new DefaultComboBoxModel<Bird>(birds);
				birdMenu.setModel(dcbm);
				cl.show(cardPanel, "answer");
			} catch (IOException e) {
				JOptionPane.showMessageDialog(frame, "Det går inte att söka nu.");
			}
    	  }
          searchInputField.setText("");
          idLabel.setText("Id:");
          nameLabel.setText("Name:");
          sortLabel.setText("Sort:");          
       }
    }
    
    /**
     * Listener for the clear button.
     * @version 2.2 2021-01-26
     * @author Daniel Viström
     */
    private class ClearListener implements ActionListener {
       
       /**
        * Creates a new ClearListener.
        */
       public ClearListener() {
           super();
       }

       /**
        * Action to perform when clear button is pressed.
        * @param event The event that happened.
        */
       public void actionPerformed(ActionEvent event){
    	   messageArea.setText(INSTRUCTION_MESSAGE);
    	   model.clearSearch();
           idLabel.setText("Id:");
           nameLabel.setText("Name:");
           sortLabel.setText("Sort:");
    	   Bird[] birds = {new Bird()};
    	   DefaultComboBoxModel<Bird> dcbm = new DefaultComboBoxModel<Bird>(birds);
    	   birdMenu.setModel(dcbm);
    	   searchInputField.requestFocusInWindow();
       }
    }
    
    private class BackListener implements ActionListener {
    	
    	public BackListener() {
    		super();
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			cl.show(cardPanel,"search");
			searchInputField.requestFocusInWindow();
		}
    	    	
    }
    
    private class SelectListener implements ActionListener {
    	
    	public SelectListener() {
    		super();
    	}

		@Override
		public void actionPerformed(ActionEvent e) {
			Bird birdSelect = birdMenu.getItemAt(birdMenu.getSelectedIndex());
			idLabel.setText("Id:"+birdSelect.getBirdId());
			nameLabel.setText("Name:"+birdSelect.getBirdName());
			sortLabel.setText("Sort:"+birdSelect.getSort());
		}
    	
    }
    
    private class LoginListener implements ActionListener {

    	public LoginListener() {
    		super();
    	}
    	
		@Override
		public void actionPerformed(ActionEvent event) {
			try {
			if(model.login(userInput.getText(), passwInput.getText())) {
				clLogin.show(cardPanelLogin, "loggedin");
				searchInputField.requestFocusInWindow();				
			}else {
				JOptionPane.showMessageDialog(frame, "Wrong login or password");
			}
			} catch(IOException e) {
				JOptionPane.showMessageDialog(frame, "Something went wrong");
			}
			userInput.setText("");
			passwInput.setText("");
		}
    	
    	
    }
    
} 	