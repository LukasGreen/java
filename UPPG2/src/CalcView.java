import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.border.CompoundBorder;

import org.w3c.dom.Text;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.LayoutManager;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

// Uppgift: Gör så att nya inlägg hamnar längst upp. 
// Du behöver ändra en rad och flytta en rad. 

/**
 * GUI for a program that displays a message.
 * 
 * @version 1.4 2023-01-27
 * @author Daniel Viström
 */
public class CalcView {

	/**
	 * Minimal frame height.
	 */
	private static final int FRAME_MIN_HEIGHT = 250;

	/**
	 * Minimal frame width.
	 */
	private static final int FRAME_MIN_WIDTH = 600;

	/**
	 * Initial frame height.
	 */
	private static final int FRAME_HEIGHT = 500;

	/**
	 * Initial frame width.
	 */
	private static final int FRAME_WIDTH = 700;

	/**
	 * Width of input field.
	 */
	private static final int INPUT_FIELD_WIDTH = 10;

	/**
	 * Width of empty border to create padding.
	 */
	private static final int PADDING_1 = 5;

	/**
	 * Width of empty border to create padding.
	 */
	private static final int PADDING_2 = 10;

	/**
	 * Message with instructions to display in the text area.
	 */
	private static final String INSTRUCTION_MESSAGE = "Enter a name to see if there is a student in TEINF3 with that name.\n";

	/**
	 * Text next to the input field.
	 */
	private static final String INPUT_LABEL_TEXT = "tis a calculator";
	/**
	 * The frame.
	 */
	private JFrame frame;

	private JTextField nameInputField;

	private JTextField nameInputField2;

	private JTextArea testMessageArea;

	private int answer;

	private JLabel inputLabelAnswer;

	/**
	 * Creates a new GUI for a program that displays a message.
	 * 
	 * @param model The model that contains the data.
	 */
	public CalcView() {
		super();
		setUpFrame();
		nameInputField.requestFocusInWindow();
		frame.setVisible(true);

	}

	/**
	 * Sets up the frame with three areas.
	 */
	private void setUpFrame() {
		frame = new JFrame("Calculator");
		frame.setSize(FRAME_WIDTH, FRAME_HEIGHT);
		frame.setMinimumSize(new Dimension(FRAME_MIN_WIDTH, FRAME_MIN_HEIGHT));
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout(new BorderLayout());
		frame.add(setUpInputPanel(), BorderLayout.CENTER);

	}

	/**
	 * Sets up an input panel with a label, an input field and a button. @ return A
	 * panel to input a search.
	 */
	private JPanel setUpInputPanel() {
		JLabel inputLabel = new JLabel(INPUT_LABEL_TEXT);
		JLabel inputLabelPlus = new JLabel("+");
		JLabel inputLabelEquals = new JLabel("=");
		inputLabelAnswer = new JLabel("");
		nameInputField = new JTextField(INPUT_FIELD_WIDTH);
		nameInputField.setBackground(Color.WHITE);
		nameInputField.setBorder(createBorder(PADDING_1));
		nameInputField.addActionListener(new InputFieldListener());

		nameInputField2 = new JTextField(INPUT_FIELD_WIDTH);
		nameInputField2.setBackground(Color.WHITE);
		nameInputField2.setBorder(createBorder(PADDING_1));
		nameInputField2.addActionListener(new InputFieldListener());

		JButton addButton = new JButton("Calculate");
		addButton.addActionListener(new InputFieldListener());

		JPanel inputPanel = new JPanel();
		inputPanel.setLayout(new FlowLayout(FlowLayout.CENTER));
		inputPanel.setBackground(Color.LIGHT_GRAY);
		inputPanel.setBorder(createBorder(PADDING_2));
		inputPanel.add(inputLabel);
		inputPanel.add(nameInputField);
		inputPanel.add(inputLabelPlus);
		inputPanel.add(nameInputField2);
		inputPanel.add(inputLabelEquals);
		inputPanel.add(inputLabelAnswer);
		inputPanel.add(addButton);
		

		return inputPanel;
	}

	/**
	 * Creates a border with an empty border as padding.
	 * 
	 * @param padd Width of empty border.
	 * @return Border with padding.
	 */
	private CompoundBorder createBorder(int padd) {
		return BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.BLACK),
				BorderFactory.createEmptyBorder(padd, padd, padd, padd));
	}

	/**
	 * Displays the message on the text area.
	 * 
	 * @param str The message to display.
	 */
	public void displayMessage(String str) {
		testMessageArea.insert(str + "\n", 0);

	}

	/**
	 * Clears the text area and writes the instruction message.
	 */
	public void clearMessage() {
		testMessageArea.setText(INSTRUCTION_MESSAGE);

	}

	/**
	 * Listener for the add button and the input field.
	 * 
	 * @version 1.4 2022-01-25
	 * @author Daniel Viström
	 */
	private class InputFieldListener implements ActionListener {

		/**
		 * Creates a new InputFieldListener.
		 */
		public InputFieldListener() {
			super();

		}

		/**
		 * Action to perform when test button or enter is pressed.
		 * 
		 * @param event The event that happened.
		 */
		@Override
		public void actionPerformed(ActionEvent e) {
			String value1 = nameInputField.getText();
			String value2 = nameInputField2.getText();
			
			answer = Integer.parseInt(value1) + Integer.parseInt(value2);
			
			inputLabelAnswer.setText("" + answer);
		}
	}

}
