/**
 * Class containing the main method.
 * @version 1.4 2023-01-27
 * @author Daniel Viström
 */
public class CalcStart {

	/**
     * Main method that starts the program.
     * @param args
     */
	public static void main(String[] args) {
		new CalcView();
	}

}
