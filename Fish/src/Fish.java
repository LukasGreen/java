
public class Fish {
	private int sort;
	private int length;
	private int weight;

	public Fish(int sort, int length, int weight) {
		this.sort = sort;
		this.length = length;
		this.weight = weight;
	}

	public int price() {
		int price = 120 * (this.weight / 1000);
		return price;
	}

	public double frakt() {

		double cost = this.length * 0.15 + weight * 0.013;

		if (cost < 12) {
			cost = 12;
		}

		return cost;
	}
}
